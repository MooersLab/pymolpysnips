# pymolpysnips: Python code snippets for the PyMOL molecular graphics program

This is a hub for the repositories related to the pymolpysnips project

## Problems addressed by this project

- **Literate programming in Jupyter notebooks with PyMOL**. The running of PyMOL in Jupyter requires use of the Python.
- **Editing live Jupyter Notebooks in an external editor** The editors is connected to the web browser via GhostText.
- **Python scripting in PyMOL**. Python scripts written for use inside of the PyMOL GUI can be more powerful than scripts written in the PyMOL macro language (pml).

## Approach to the problem

-  Lower the barrier to entry by providing a library of Python code snippets that accomplish common and not so common tasks.
-  Make the library available for several kinds of computational platforms to support users in their favorite computational environments.

## Related 

<a id="pymolpysnips-for-notebooks"><h4>For use in notebooks</h4></a>
- [tagged pymolpy snippets ](https://codeberg.org/MooersLab/taggedcpymolpysnips) for JupyterLab *elyra-code-snippets* extension
- [tagged pymolpy snippets plus](https://codeberg.org/MooersLab/taggedpymolpysnipsplus) plus variant for JupyterLab *elyra-code-snippets* extension
- [Jupyterlab pymolpy snippets](https://codeberg.org/MooersLab/jupyterlabpymolpysnips) for the *jupyterlab-snippets* extension or the jupyterlab-snippets-mutlimenus extension.
- [Jupyterlab pymolpy plus snippets](https://codeberg.org/MooersLab/jupyterlabpymolpysnipsplus), the variant of the *jupyterlabpymolpysnips* library with comments to guide editing of the snippets.

<a id="pymolpysnips-for-editors"><h4>For use in text editors</h4></a>
- [pymolpysnips-Emacs](https://codeberg.org/MooersLab/pymolpysnips-Emacs) for the *yasnippet* snippet system in for Emacs.
- [pymolpysnips-VSC](https://codeberg.org/MooersLab/pymolpysnipss-VSC) for the Visual Studio Code (VSC) editor.
- [pymolpysnips-SublimeText3](https://codeberg.orgMooersLab/pymolpysnips-SublimeText3) for the Sublime Text 3 (ST3) editor.
- [pymolpysnips-UltiSnips](https://codeberg.org/MooersLab/pymolpysnips-Ultisnips) for Vim or NeoVim via UltiSnips plugin.
- [pymolpysnips-neosnippets](https://codeberg.org/MooersLab/pymolpysnips-neosnippets) for Vim or NeoVim via neosnippets plugin.
- [pymolpysnips-Snipmate](https://codeberg.orgMooersLab/pymolpysnips-snipmate) for Vim or NeoVim via snipmate plugin.
- [pymolpysnips-Atom](https://codeberg.org/MooersLab/pymolpysnips-Atom) for Atom.

